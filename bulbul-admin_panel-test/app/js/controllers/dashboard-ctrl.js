App.controller('DashboardController', function ($scope, $http, $cookies, $cookieStore, MY_CONSTANT, ngDialog) {
    'use strict';
    $scope.loading = true;
    $scope.current = {};
    $scope.gross = {};
    $scope.registered = {};
    $.post(MY_CONSTANT.url + '/dashboard', {
        access_token: $cookieStore.get('obj').accesstoken

    }, function (data) {
        console.log(data);
        data = JSON.parse(data);
        if (data.error) {
            ngDialog.open({
                template: '<p>Something went wrong !</p>',
                className: 'ngdialog-theme-default',
                plain: true,
                showClose: false,
                closeByDocument: false,
                closeByEscape: false
            });
            return false;
        }
        console.log(data);
        $scope.current.totalrevenue = data.total_current_booking;
        $scope.current.servicescompleted = data.complete_current_booking;
        $scope.current.servicesscheduled = data.uncomplete_current_booking;
        $scope.current.cancelSchedule = data.cancel_current_booking;

        $scope.gross.totalrevenue = data.total_booking;
        $scope.gross.servicescompleted = data.complete_booking;
        $scope.gross.servicesscheduled = data.unComplete_booking;
        $scope.gross.cancelSchedule = data.cancel_booking;

        $scope.registered.users = data.total_user;
        $scope.registered.sp = data.total_tech;
        $scope.registered.today = data.today_all_register;
        $scope.$apply();

    });

});