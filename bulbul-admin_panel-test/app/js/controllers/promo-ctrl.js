App.controller('ActivePromoController', function ($scope, $http, $route, $cookieStore, $timeout, $location, MY_CONSTANT, convertdatetime, $window, ngDialog) {

    var getPromo = function () {
        $.post(MY_CONSTANT.url + '/promotion_code_list', {
            access_token: $cookieStore.get('obj').accesstoken

        }, function (data) {
            var dataArray = [];
            data = JSON.parse(data);
            console.log(data);
            if (data.error) {
                ngDialog.open({
                    template: '<p>Something went wrong !</p>',
                    className: 'ngdialog-theme-default',
                    plain: true,
                    showClose: false,
                    closeByDocument: false,
                    closeByEscape: false
                });
                return false;
            }
            data = data.active_codes;
            data.forEach(function (column) {

                var d = {
                    code_id: "",
                    user_id: "",
                    code: "",
                    credits: "",
                    create_date: "",
                    start_date: "",
                    expiry_date: ""
                };

                d.user_id = column.user_id;
                d.code_id = column.code_id;
                d.code = column.code;
                d.credits = column.credits;
                var create_date = column.created_time.toString().split("T")[0];
                d.create_date = create_date;
                var start_date = column.start_date.toString().split("T")[0];
                d.start_date = start_date;
                var expiry_date = column.expiry_date.toString().split("T")[0];
                d.expiry_date = expiry_date;
                dataArray.push(d);

            });

            $scope.$apply(function () {
                $scope.list = dataArray;


                // Define global instance we'll use to destroy later
                var dtInstance;
                $scope.loading = false;
                $timeout(function () {
                    if (!$.fn.dataTable)
                        return;
                    dtInstance = $('#datatable2').dataTable({
                        'paging': true, // Table pagination
                        'ordering': true, // Column ordering
                        'info': true, // Bottom left status text
                        'bDestroy': true,
                        // Text translation options
                        // Note the required keywords between underscores (e.g _MENU_)
                        oLanguage: {
                            sSearch: 'Search all columns:',
                            sLengthMenu: '_MENU_ records per page',
                            info: 'Showing page _PAGE_ of _PAGES_',
                            zeroRecords: 'Nothing found - sorry',
                            infoEmpty: 'No records available',
                            infoFiltered: '(filtered from _MAX_ total records)'
                        }
                    });
                    var inputSearchClass = 'datatable_input_col_search';
                    var columnInputs = $('tfoot .' + inputSearchClass);

                    // On input keyup trigger filtering
                    columnInputs
                        .keyup(function () {
                            dtInstance.fnFilter(this.value, columnInputs.index(this));
                        });
                });

                // When scope is destroyed we unload all DT instances
                // Also ColVis requires special attention since it attaches
                // elements to body and will not be removed after unload DT
                $scope.$on('$destroy', function () {
                    dtInstance.fnDestroy();
                    $('[class*=ColVis]').remove();
                });
            });

        });
    };

    getPromo();

    $scope.promo = {};
    $scope.checkDate = function () {
        if ($scope.promo.end_date < $scope.promo.start_date) {
            ngDialog.open({
                template: '<p class="del-dialog">End date must be greater than start date !!</p>',
                plain: true,
                className: 'ngdialog-theme-default'

            });
            return false;
        }
    }

    $scope.addPromo = function () {


        if ($scope.promo.start_date == undefined) {
            ngDialog.open({
                template: '<p class="del-dialog">Please select start date !!</p>',
                plain: true,
                className: 'ngdialog-theme-default'

            });
            return false;
        }
        else if ($scope.promo.end_date == undefined) {
            ngDialog.open({
                template: '<p class="del-dialog">Please select end date !!</p>',
                plain: true,
                className: 'ngdialog-theme-default'

            });
            return false;
        }
        else if ($scope.promo.credits == undefined) {
            ngDialog.open({
                template: '<p class="del-dialog">Please fill credits first !!</p>',
                plain: true,
                className: 'ngdialog-theme-default'

            });
            return false;
        }
        if ($scope.promo.end_date < $scope.promo.start_date) {
            ngDialog.open({
                template: '<p class="del-dialog">End date must be greater than start date !!</p>',
                plain: true,
                className: 'ngdialog-theme-default'

            });
            return false;
        }

        var start_date = convertdatetime.convertDate($scope.promo.start_date);
        var to_date = convertdatetime.convertDate($scope.promo.end_date);

        $.post(MY_CONSTANT.url + '/add_promo_code',
            {
                access_token: $cookieStore.get('obj').accesstoken,
                credits: $scope.promo.credits,
                start_date: start_date,
                expiry_date: to_date
            },
            function (data) {

                data = JSON.parse(data);
                console.log(data);
                if (data.log) {
                    alert(data.log);
                    $window.location.reload();
                }
                else {
                    alert("Promo code can't be added right now !!");
                }
            });


    };

    // Delete Dialog
    $scope.deletePromo = function (userid) {
        $scope.dele_val = userid;
        $scope.value = true;
        $scope.addTeam = {};
        ngDialog.open({
            template: 'app/views/delete-dialog.html',
            className: 'ngdialog-theme-default',
            scope: $scope
        });
    };

    $scope.delete = function () {

        $.post(MY_CONSTANT.url + '/delete_promotion_code',
            {
                access_token: $cookieStore.get('obj').accesstoken,
                code_id: $scope.dele_val
            },
            function (data) {
                $window.location.reload();

            });

    };

});

App.controller('ExpiredPromoController', function ($scope, $http, $route, $cookieStore, $timeout, $location, MY_CONSTANT, $window, ngDialog) {
    var getPromo = function () {
        $.post(MY_CONSTANT.url + '/promotion_code_list', {
            access_token: $cookieStore.get('obj').accesstoken

        }, function (data) {
            var dataArray = [];
            data = JSON.parse(data);
            console.log(data);
            if (data.error) {
                ngDialog.open({
                    template: '<p>Something went wrong !</p>',
                    className: 'ngdialog-theme-default',
                    plain: true,
                    showClose: false,
                    closeByDocument: false,
                    closeByEscape: false
                });
                return false;
            }
            data = data.expired_codes;
            data.forEach(function (column) {

                var d = {
                    code_id: "",
                    user_id: "",
                    code: "",
                    credits: "",
                    create_date: "",
                    start_date: "",
                    expiry_date: ""
                };

                d.user_id = column.user_id;
                d.code_id = column.code_id;
                d.code = column.code;
                d.credits = column.credits;
                var create_date = column.created_time.toString().split("T")[0];
                d.create_date = create_date;
                var start_date = column.start_date.toString().split("T")[0];
                d.start_date = start_date;
                var expiry_date = column.expiry_date.toString().split("T")[0];
                d.expiry_date = expiry_date;
                dataArray.push(d);

            });

            $scope.$apply(function () {
                $scope.list = dataArray;


                // Define global instance we'll use to destroy later
                var dtInstance;
                $scope.loading = false;
                $timeout(function () {
                    if (!$.fn.dataTable)
                        return;
                    dtInstance = $('#datatable2').dataTable({
                        'paging': true, // Table pagination
                        'ordering': true, // Column ordering
                        'info': true, // Bottom left status text
                        'bDestroy': true,
                        // Text translation options
                        // Note the required keywords between underscores (e.g _MENU_)
                        oLanguage: {
                            sSearch: 'Search all columns:',
                            sLengthMenu: '_MENU_ records per page',
                            info: 'Showing page _PAGE_ of _PAGES_',
                            zeroRecords: 'Nothing found - sorry',
                            infoEmpty: 'No records available',
                            infoFiltered: '(filtered from _MAX_ total records)'
                        }
                    });
                    var inputSearchClass = 'datatable_input_col_search';
                    var columnInputs = $('tfoot .' + inputSearchClass);

                    // On input keyup trigger filtering
                    columnInputs
                        .keyup(function () {
                            dtInstance.fnFilter(this.value, columnInputs.index(this));
                        });
                });

                // When scope is destroyed we unload all DT instances
                // Also ColVis requires special attention since it attaches
                // elements to body and will not be removed after unload DT
                $scope.$on('$destroy', function () {
                    dtInstance.fnDestroy();
                    $('[class*=ColVis]').remove();
                });
            });

        });
    };

    getPromo();

    // Delete Dialog
    $scope.deletePromo = function (userid) {
        $scope.dele_val = userid;
        $scope.value = true;
        $scope.addTeam = {};
        ngDialog.open({
            template: 'app/views/delete-dialog.html',
            className: 'ngdialog-theme-default',
            scope: $scope
        });
    };

    $scope.delete = function () {

        $.post(MY_CONSTANT.url + '/delete_promotion_code',
            {
                access_token: $cookieStore.get('obj').accesstoken,
                code_id: $scope.dele_val
            },
            function (data) {
                $window.location.reload();

            });

    };

});

App.controller('ItemController', ['$scope', function ($scope) {

    $scope.$parent.isopen = ($scope.$parent.default === $scope.item);

    $scope.$watch('isopen', function (newvalue, oldvalue, scope) {
        $scope.$parent.isopen = newvalue;
    });

}]);

App.controller('SpecialOfferPromoCode', function ($scope, $http, $route, $cookieStore, $timeout, $location, MY_CONSTANT, convertdatetime, $window, ngDialog,$filter,checkServiceName) {

     getAllService();

    $scope.add = {};
    $scope.promo = {};
    $scope.quantityValue1 = 1;
    $scope.quantityValue = 1;
    $scope.add.offerPromoCode=1;
    $scope.singleServices=true;
    $scope.multipleServices=true;
    $scope.creditsCheck=true;
    $scope.serviceId = true;
    $scope.comboId = true;
    $scope.quantity ={};
    $scope.packageDescription = true;
    $scope.SpecialOfferflagValue = true;
    $scope.minDate=new Date();
    $scope.minDate1= new Date();
    $scope.specialOfferFlag = 0;

    $scope.promo.start_date = $filter('date')(new Date(),'yyyy-MM-dd');
    $scope.promo.end_date = $filter('date')(new Date(),'yyyy-MM-dd');
    //console.log("start and end date");
    //console.log($scope.promo.start_date,$scope.promo.end_date);
    console.log($scope.add.offerPromoCode);

    $scope.startDate = function(){
        $scope.promo.start_date = $filter('date')($scope.promo.start_date,'yyyy-MM-dd');
    }
    $scope.endDate = function(){
        $scope.promo.end_date = $filter('date')($scope.promo.end_date,'yyyy-MM-dd');
    }
    $scope.list2 = $scope.list1;


    $scope.offer_promo_code=function(){

        if($scope.add.offerPromoCode=="1"){
            $scope.singleServices=true;
            $scope.multipleServices=true;
            $scope.creditsCheck=true;
            $scope.discountCheck=false;
            $scope.serviceId = true;
            $scope.comboId = true;
            $scope.packageDescription = true;
            $scope.SpecialOfferflagValue = true;
            $scope.serviceName = "";
            getPromo(1);
        }
        if($scope.add.offerPromoCode=="2"){
            $scope.singleServices=true;
            $scope.multipleServices=true;
            $scope.discountCheck=false;
            $scope.creditsCheck=false;
            $scope.serviceId = true;
            $scope.comboId = true;
            $scope.packageDescription = true;
            $scope.SpecialOfferflagValue = true;
            $scope.serviceName = "";
            getPromo(2);
        }
        if($scope.add.offerPromoCode=="3"){
            $scope.serviceId=false;
            $scope.comboId = true;
            getPromo(3);
            $scope.singleServices=false;
            $scope.multipleServices=true;
            $scope.discountCheck=true;
            $scope.packageDescription = true;
            $scope.creditsCheck=false;
            $scope.SpecialOfferflagValue = false;
            $scope.serviceName = "";
            if($scope.checkedValue){
                $scope.checkedValue = !$scope.checkedValue;
            }
            if($scope.serviceType){
                $scope.serviceType = !$scope.serviceType;
            }
        }
        if($scope.add.offerPromoCode=="4"){
            $scope.serviceId=false;
            $scope.comboId = true;
            getPromo(4);
            $scope.singleServices=false;
            $scope.multipleServices=true;
            $scope.packageDescription = true;
            $scope.discountCheck=false;
            $scope.creditsCheck=false;
            $scope.SpecialOfferflagValue = false;
            $scope.serviceName = "";
            if($scope.checkedValue){
                $scope.checkedValue = !$scope.checkedValue;
            }
            if($scope.serviceType){
                $scope.serviceType = !$scope.serviceType;
            }
        }
        if($scope.add.offerPromoCode=="5"){

            $scope.singleServices=true;
            $scope.multipleServices=false;
            $scope.discountCheck=true;
            $scope.creditsCheck=false;
            $scope.packageDescription = true;
            $scope.serviceId=true;
            $scope.comboId = false;
            $scope.SpecialOfferflagValue = false;
            $scope.serviceName = "";
            if($scope.checkedValue){
                $scope.checkedValue = !$scope.checkedValue;
            }
            getPromo(5);
        }
        if($scope.add.offerPromoCode=="6"){
            $scope.singleServices=true;
            $scope.multipleServices=false;
            $scope.discountCheck=false;
            $scope.creditsCheck=false;
            $scope.packageDescription = true;
            $scope.serviceId=true;
            $scope.comboId = false;
            $scope.SpecialOfferflagValue = false;
            $scope.serviceName = "";
            if($scope.checkedValue){
                $scope.checkedValue = !$scope.checkedValue;
            }
            getPromo(6);
        }
    }
    $scope.worthPrice2 = 0;

    $scope.setChoiceForService =function(arr,choice){
        $scope.serviceType = choice.id;
        $scope.serviceName = choice.service_name;
        $scope.worthPrice2 = choice.price;
        $scope.value = true;

        ngDialog.open({
            template: '<h3>Enter the Quantity Of Service</h3><input type="number" class="form-control" min="1" max="5" value="1" ng-model="quantityValue1"><input type="submit" class="btn btn-primary" ng-click="submitQuantity(quantityValue1)" style="margin-top: 10px;background-color: #f532e5;">',
            plain: true,
            className: 'ngdialog-theme-default',
            scope:$scope
        });
    }
    $scope.submitQuantity = function(value){
        console.log(value);
        ngDialog.close();
        $scope.singleQuantity=value;
    }

    $scope.getCheckBoxValue = function(){
        console.log($scope.checkedValue)
         if($scope.checkedValue){
            $scope.specialOfferFlag=1;
             $scope.packageDescription = false;
         }
        else{
             $scope.specialOfferFlag=0;
             $scope.packageDescription = true;
         }

    }

    $scope.addPromo=function(){

        var flag=$scope.add.offerPromoCode;
        var comboServicesArray=[];
        $scope.serviceName1 = [];
        servicesQuantity =[];
       /* var selectCity =[];*/
        console.log($scope.quantity.item1);

       /* if ($scope.quantity.item1 == undefined) {
            openDialogBox("Please select quantity of each services !!");
            return false;
        }*/

        for(var i=0;i<$scope.list1.length;i++){
            if($scope.list1[i].data==1){
                comboServicesArray.push($scope.list1[i].id);
                console.log($scope.quantity.length);
                if($scope.quantity.item1[$scope.list1[i].id]==""||$scope.quantity.item1[$scope.list1[i].id]==undefined){
                    openDialogBox("Please select quantity of each services !!");
                    return false;
                }
                servicesQuantity.push($scope.quantity.item1[$scope.list1[i].id]);
                $scope.serviceName1.push($scope.list1[i].service_name);
               $scope.worthPrice2 = $scope.worthPrice2 + $scope.list1[i].price;
            }
        }
       /* for(var k=0;k<$scope.cityList.length;k++){
            if($scope.cityList[k].data==1){
                selectCity.push($scope.cityList[k].city_name);
            }
        }*/
     /*   console.log("add the select city");
        console.log(selectCity);*/



      //  console.log($scope.serviceName);
       // console.log($scope.singleQuantity);
      //  console.log($scope.worthPrice2);
        $scope.serviceName1 = $scope.serviceName1.join(',').replace(/,/g, ' & ').split();
        if ($scope.promo.start_date == undefined) {
            openDialogBox("Please select start date !!");
            return false;
        }
        else if ($scope.promo.end_date == undefined) {
            openDialogBox("Please select end date !!");
            return false;
        }
        if($scope.code == undefined){
            openDialogBox("Please fill Promo Code !!");
            return false;
        }
       /* console.log($scope.promo.end_date<$scope.promo.start_date);
        console.log($scope.promo.end_date,$scope.promo.start_date);*/
        if ($scope.promo.end_date < $scope.promo.start_date) {
            openDialogBox("End date must be greater than start date !!");
            return false;
        }
        if(flag==1){
             if ($scope.discount == undefined) {
                 openDialogBox("Please fill discount first !!");
                 return false;
            }
        }
        if(flag==2){
            if ($scope.discount == undefined) {
                openDialogBox("Please fill discount first !!");
                return false;
            }
        }
        if(flag==3){
            if ($scope.credits == undefined) {
                openDialogBox("Please fill credits first !!");
                return false;
            }
            if ($scope.serviceType == undefined) {
                openDialogBox("Please select the service option first !!");
                return false;
            }
        }
        if(flag==4){
            if ($scope.credits == undefined) {
                openDialogBox("Please fill credits first !!");
                return false;
            }
            if ($scope.discount == undefined) {
                openDialogBox("Please fill discount first !!");
                return false;
            }
            if ($scope.serviceType == undefined) {
                openDialogBox("Please select the service option first !!");
                return false;
            }
        }
        if(flag==5){
            if ($scope.credits == undefined) {
                openDialogBox("Please fill credits first !!");
                return false;
            }
            if (comboServicesArray== undefined||comboServicesArray.length==0) {
                openDialogBox("Please select the service option first !!");
                return false;
            }
        }
        if(flag==6){
            if ($scope.credits == undefined) {
                openDialogBox("Please fill credits first !!");
                return false;
            }
            if ($scope.discount == undefined) {
                openDialogBox("Please fill discount first !!");
                return false;
            }
            if (comboServicesArray== undefined||comboServicesArray.length==0) {
                openDialogBox("Please select the service option first !!");
                return false;
            }
        }
        if ($scope.bridalPackage == undefined) {
          /*  openDialogBox("Please fill the bridal package title !!");
            return false;*/
            $scope.bridalPackage = "";
        }
        /*if(selectCity.length==0){
            openDialogBox("Please select the city where apply the promo code !!");
            return false;
        }*/

        var service_name="";
        var service_quantity = "";
         if($scope.add.offerPromoCode==3||$scope.add.offerPromoCode==4){
             service_name = $scope.serviceName;
             service_quantity = $scope.singleQuantity;
         }
        if($scope.add.offerPromoCode==5||$scope.add.offerPromoCode==6){
            service_name = $scope.serviceName1;
            service_quantity = servicesQuantity;
        }
        console.log("special offer flag"+$scope.specialOfferFlag);
       // console.log($scope.start_date,$scope.end_date,$scope.discount,$scope.serviceType);
        $.post(MY_CONSTANT.url + '/add_special_offers', {
            access_token: $cookieStore.get('obj').accesstoken,
            start_date:$scope.promo.start_date,
            expiry_date:$scope.promo.end_date,
            flag:$scope.add.offerPromoCode,
            price:$scope.credits,
            discount:$scope.discount,
            code:$scope.code,
            service_type:$scope.serviceType,
            combo_services:comboServicesArray,
            service_name:service_name,
            special_offer_flag:$scope.specialOfferFlag,
            worth_price:$scope.worthPrice2,
            quantity_of_services:service_quantity,
            bridal_package_title:$scope.bridalPackage,
            city_name : $scope.cityName
           /* city_name: selectCity
*/
        }, function (data) {
            data = JSON.parse(data);
            console.log(data);
            if(data.log){
                ngDialog.open({
                    template: '<p class="del-dialog">'+data.log+'</p>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                });
                $timeout(function() {
                    location.reload(true);
                }, 1000);
            }
            else{
                ngDialog.open({
                    template: '<p class="del-dialog">'+data.error+'</p>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                });
                return false;
            }
        });

     function  openDialogBox(message){
            ngDialog.open({
                template: '<p class="del-dialog">'+message+'</p>',
                plain: true,
                className: 'ngdialog-theme-default'
            });
        }

    }



    var getPromo = function (flag) {

        $.post(MY_CONSTANT.url + '/list_of_special_offer', {
            access_token: $cookieStore.get('obj').accesstoken,
            flag: flag

        }, function (data) {
            var dataArray = [];
            data = JSON.parse(data);
            console.log(data);
            if (data.error) {
                ngDialog.open({
                    template: '<p>Something went wrong !</p>',
                    className: 'ngdialog-theme-default',
                    plain: true,
                    showClose: false,
                    closeByDocument: false,
                    closeByEscape: false
                });
                return false;
            }

            data.forEach(function (column) {

                var d = {
                    code_id: "",
                    code: "",
                    price: "",
                    create_date: "",
                    start_date: "",
                    expiry_date: "",
                    discount:"",
                    description:"",
                    package_offer_flag:""
                };

                d.code_id = column.code_id;
                d.code = column.code;
                d.price = column.service_rate;
                d.discount = column.discount;
                var create_date = column.created_time.toString().split("T")[0];
                d.create_date = create_date;
                var start_date = column.start_date.toString().split("T")[0];
                d.start_date = start_date;
                var expiry_date = column.expiry_date.toString().split("T")[0];
                d.expiry_date = expiry_date;
                d.description = column.description;
                d.package_offer_flag = column.package_offer_flag;
                dataArray.push(d);

            });
            $scope.$apply(function () {

                if ($.fn.DataTable.isDataTable("#datatable2")) {
                    console.log("hello");
                    $('#datatable2').DataTable().clear().destroy();
                }

                $scope.list = dataArray;


                // Define global instance we'll use to destroy later
                var dtInstance;
                $scope.loading = false;
                $timeout(function () {
                    if (!$.fn.dataTable)
                        return;
                    dtInstance = $('#datatable2').dataTable({
                        'paging': true, // Table pagination
                        'ordering': true, // Column ordering
                        'info': true, // Bottom left status text
                        'bDestroy': true,
                        // Text translation options
                        // Note the required keywords between underscores (e.g _MENU_)
                        oLanguage: {
                            sSearch: 'Search all columns:',
                            sLengthMenu: '_MENU_ records per page',
                            info: 'Showing page _PAGE_ of _PAGES_',
                            zeroRecords: 'Nothing found - sorry',
                            infoEmpty: 'No records available',
                            infoFiltered: '(filtered from _MAX_ total records)'
                        }
                    });
                    var inputSearchClass = 'datatable_input_col_search';
                    var columnInputs = $('tfoot .' + inputSearchClass);

                    // On input keyup trigger filtering
                    columnInputs
                        .keyup(function () {
                            dtInstance.fnFilter(this.value, columnInputs.index(this));
                        });
                });

                // When scope is destroyed we unload all DT instances
                // Also ColVis requires special attention since it attaches
                // elements to body and will not be removed after unload DT
                $scope.$on('$destroy', function () {
                    dtInstance.fnDestroy();
                    $('[class*=ColVis]').remove();
                });
            });

        });
    };

    function getAllService(){
        $.post(MY_CONSTANT.url + '/get_all_service', {
            access_token: $cookieStore.get('obj').accesstoken

        }, function (data) {
            data = JSON.parse(data);
            console.log(data);
            if (data.error) {
                ngDialog.open({
                    template: '<p>Something went wrong !</p>',
                    className: 'ngdialog-theme-default',
                    plain: true,
                    showClose: false,
                    closeByDocument: false,
                    closeByEscape: false
                });
                return false;
            }
            else{
                for(var i=0;i<data.length;i++){
                    data[i].data=0;
                }
                console.log(data);
                /*return data;*/
                $scope.list1 = data;
            }
        });
    }

    $scope.deletePromo = function (userid) {
        $scope.dele_val = userid;
        $scope.value = true;
        $scope.addTeam = {};
        ngDialog.open({
            template: 'app/views/delete-dialog.html',
            className: 'ngdialog-theme-default',
            scope: $scope
        });
    };

    $scope.delete = function () {

        $.post(MY_CONSTANT.url + '/delete_special_offer',
            {
                access_token: $cookieStore.get('obj').accesstoken,
                code_id: $scope.dele_val
            },
            function (data) {
                $window.location.reload();

            });

    };

    getPromo(1);

    /*$scope.cityList = [
             {
                 id :1,
                 city_name : "Chandigarh",
                 data : "0"
             },
             {
                 id :2,
                 city_name : "Mohali",
                 data : "0"
             },
             {
                 id :3,
                 city_name : "Zirakpur",
                 data : "0"
             },
             {
                 id :4,
                 city_name : "Panchkula",
                 data : "0"
             },
             {
                 id :5,
                 city_name : "Zirakpur",
                 data : "0"
             },
             {
                 id :6,
                 city_name : "Delhi",
                 data : "0"
             }
        ]
*/

});