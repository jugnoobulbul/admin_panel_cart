/**
 * Created by Chirag on 30/06/15.
 */
App.controller('PushController', function ($scope, $http, $location, $cookies, $cookieStore, MY_CONSTANT, ngDialog, $timeout) {
    'use strict';
   // $scope.loading = true;

    $scope.image1={};
    $scope.chooseOfferFlag = 0;
    $scope.file_to_upload = function (File) {
        $scope.FileUploaded = File[0];
        $scope.image1 = File[0];
    };
    var flagValue = 0;

    $scope.chooseOffer = function(){
        console.log($scope.chooseOfferFlag);
        flagValue = $scope.chooseOfferFlag;
    }

   $scope.sendPushNotification=function(){

       if($scope.notification_content==undefined||$scope.notification_content==""){
           ngDialog.open({
               template: '<p class="del-dialog">Plz enter the content for sending the notification !!</p>',
               plain: true,
               className: 'ngdialog-theme-default'

           });
           return false;
       }
       else if($scope.title==undefined||$scope.title==""){
           ngDialog.open({
               template: '<p class="del-dialog">Plz enter the title for the push notification !!</p>',
               plain: true,
               className: 'ngdialog-theme-default'

           });
           return false;
       }
       else {

           var formData = new FormData();
           formData.append('access_token', $cookieStore.get('obj').accesstoken);
           formData.append('notification_message', $scope.notification_content);
           formData.append('title',$scope.title);
           formData.append('flag',flagValue);
           formData.append('image',$scope.image1);

           console.log(formData);
           $.ajax({
               type: "POST",
               url: MY_CONSTANT.url + '/send_push_notification',
               dataType: "json",
               data: formData,
               async: false,
               processData: false,
               contentType: false,
               success: function (data) {
                   console.log(data);
                  /* data = JSON.parse(data);
                   console.log(data);*/
                   if(data.error)
                   {
                       ngDialog.open({
                           template: data.error,
                           plain: true,
                           className: 'ngdialog-theme-default'

                       });
                       return false;
                   }
                   else{
                       ngDialog.open({
                           template: data.log,
                           plain: true,
                           className: 'ngdialog-theme-default'
                       });
                       $timeout(function() {
                           location.reload(true);
                       }, 3000);
                   }
               }

           });
           /*$.post(MY_CONSTANT.url + '/send_push_notification', {
               access_token: $cookieStore.get('obj').accesstoken,
               notification_message: $scope.notification_content,
               title: $scope.title

           }, function (data) {
               var dataArray = [];
               data = JSON.parse(data);
               console.log(data);
               if (data.error) {
                   ngDialog.open({
                       template: data.error,
                       plain: true,
                       className: 'ngdialog-theme-default'

                   });
                   return false;
               }
               else {
                   ngDialog.open({
                       template: data.log,
                       plain: true,
                       className: 'ngdialog-theme-default'
                   });
                   $timeout(function() {
                       location.reload(true);
                   }, 3000);
               }
           });*/
       }
   }
});

App.controller('BeautyTipsController', function ($scope, $http, $location, $cookies, $cookieStore, MY_CONSTANT, ngDialog, $timeout) {
    'use strict';
    // $scope.loading = true;
    $scope.addBeautyTips=function(){

        if($scope.titleTips==undefined||$scope.titleTips==""){
            ngDialog.open({
                template: '<p class="del-dialog">Plz add the title for the Beauty Tips !!</p>',
                plain: true,
                className: 'ngdialog-theme-default'

            });
            return false;
        }
        else if($scope.messageTips==undefined||$scope.messageTips==""){
            ngDialog.open({
                template: '<p class="del-dialog">Plz add the message for Beauty Tips and Offer Message</p>',
                plain: true,
                className: 'ngdialog-theme-default'

            });
            return false;
        }
        else {

            $.post(MY_CONSTANT.url + '/add_beauty_tips_and_offer', {
                access_token: $cookieStore.get('obj').accesstoken,
                message_tips: $scope.messageTips,
                title_tips: $scope.titleTips

            }, function (data) {
                var dataArray = [];
                data = JSON.parse(data);
                console.log(data);
                if (data.error) {
                    ngDialog.open({
                        template: data.error,
                        plain: true,
                        className: 'ngdialog-theme-default'

                    });
                    return false;
                }
                else {
                    ngDialog.open({
                        template: data.log,
                        plain: true,
                        className: 'ngdialog-theme-default'
                    });
                    $timeout(function() {
                        location.reload(true);
                    }, 3000);
                }
            });
        }
    }
});

App.controller('TablexEditableController', function($scope, $filter, $http, $cookies, $cookieStore, editableOptions,$timeout, editableThemes, $q,MY_CONSTANT,ngDialog) {


        getBeautyTipsAndOffer();



        function getBeautyTipsAndOffer(){
            $.post(MY_CONSTANT.url + '/get_beauty_tips_and_offer', {
                access_token: $cookieStore.get('obj').accesstoken
            }, function (data) {
                var dataArray = [];
                data = JSON.parse(data);
                console.log(data);
                if (data.error) {
                    ngDialog.open({
                        template: '<p>Something went wrong !</p>',
                        className: 'ngdialog-theme-default',
                        plain: true,
                        showClose: true,
                        closeByDocument: false,
                        closeByEscape: false
                    });
                    return false;
                }
                data.forEach(function (column) {

                    var d = {
                        id:"",
                        title: "",
                        message: ""
                    };
                    d.id = column.id;
                    d.title = column.title;
                    d.message = column.message;
                    dataArray.push(d);

                });

                $scope.$apply(function () {
                    $scope.list = dataArray;

                });

                console.log(data);
            });
        }


       /* $scope.users = [
            {id: 1, name: 'awesome user1', status: 2, group: 4, groupName: 'admin'},
            {id: 2, name: 'awesome user2', status: undefined, group: 3, groupName: 'vip'},
            {id: 3, name: 'awesome user3', status: 2, group: null}
        ];

        $scope.statuses = [
            {value: 1, text: 'status1'},
            {value: 2, text: 'status2'},
            {value: 3, text: 'status3'},
            {value: 4, text: 'status4'}
        ];*/

      /*  $scope.groups = [];
        $scope.loadGroups = function() {
            return $scope.groups.length ? null : $http.get('server/xeditable-groups.json').success(function(data) {
                $scope.groups = data;
            });
        };*/

        $scope.showGroup = function(user) {
            if(user.group && $scope.groups.length) {
                var selected = $filter('filter')($scope.groups, {id: user.group});
                return selected.length ? selected[0].text : 'Not set';
            } else {
                return user.groupName || 'Not set';
            }
        };

        $scope.showStatus = function(user) {
            var selected = [];
            if(user.status) {
                selected = $filter('filter')($scope.statuses, {value: user.status});
            }
            return selected.length ? selected[0].text : 'Not set';
        };

        $scope.checkName = function(data, id) {
            if (id === 2 && data !== 'awesome') {
                return "Username 2 should be `awesome`";
            }
        };

        $scope.saveMessage = function(data, id) {
            //$scope.user not updated yet
            console.log(data);
            $.post(MY_CONSTANT.url + '/update_beauty_tips', {
                access_token: $cookieStore.get('obj').accesstoken,
                title: data.title,
                message:data.message,
                id:id
            }, function (data) {
                data = JSON.parse(data);
                angular.extend(data, {id: id});
                ngDialog.open({
                    template: data.log,
                    plain: true,
                    className: 'ngdialog-theme-default'
                });
                $timeout(function() {
                    location.reload(true);
                }, 1000);

            });

            // return $http.post('/saveUser', data);
        };

        // remove user
        $scope.removeMessage = function(index) {
         //   $scope.users.splice(index, 1);
            console.log(index)
            $.post(MY_CONSTANT.url + '/delete_beauty_tips', {
                access_token: $cookieStore.get('obj').accesstoken,
                id:index
            }, function (data) {
                data = JSON.parse(data);
                ngDialog.open({
                    template: data.log,
                    plain: true,
                    className: 'ngdialog-theme-default'
                });
                $timeout(function() {
                    location.reload(true);
                }, 3000);

            });
        };
    });