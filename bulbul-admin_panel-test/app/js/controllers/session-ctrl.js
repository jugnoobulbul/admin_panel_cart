App.controller('OngoingSessionController', function ($scope, $http, $location, $cookies, $cookieStore, MY_CONSTANT, $timeout, $window, ngDialog) {
    'use strict';
    $scope.loading = true;

    var getServiceList = function () {
        $.post(MY_CONSTANT.url + '/ongoing_booking', {
            access_token: $cookieStore.get('obj').accesstoken

        }, function (data) {
            var dataArray = [];
            data = JSON.parse(data);
            console.log(data);
            if (data.error) {
                ngDialog.open({
                    template: '<p>Something went wrong !</p>',
                    className: 'ngdialog-theme-default',
                    plain: true,
                    showClose: false,
                    closeByDocument: false,
                    closeByEscape: false
                });
                return false;
            }
            data.forEach(function (column) {

                var d = {
                    id: "",
                    booking_id: "",
                    service_id: "",
                    technician_id: "",
                    address: "",
                    customer_name: "",
                    artist_name: "",
                    service_date: "",
                    start_time: "",
                    status: "",
                    cost: "",
                    payable_amount: "",
                    category: "",
                    service_name: ""
                };

                var date = column.service_date.toString().split("T")[0];
                var startTimeHours = column.start_time.split(":")[0];
                var startTimeMinutes = column.start_time.split(":")[1];
                var suffix = startTimeHours >= 12 ? "PM" : "AM",
                    hours12 = startTimeHours % 12;
                var displayTime = hours12 + ":" + startTimeMinutes + " " + suffix;
                d.id = column.id;
                d.booking_id = column.booking_id;
                d.service_id = column.service_id;
                d.technician_id = column.technician_id;
                d.address = column.address;
                d.customer_name = column.customer_name;
                d.artist_name = column.artist_name;
                d.start_time = displayTime;
                d.cost = column.cost;
                d.payable_amount = column.payable_amount;
                d.category = column.category;
                d.service_date = date;
                d.status = column.status;
                d.service_name = column.service_name;
                dataArray.push(d);

            });

            $scope.$apply(function () {
                $scope.list = dataArray;


                // Define global instance we'll use to destroy later
                var dtInstance;
                $scope.loading = false;
                $timeout(function () {
                    if (!$.fn.dataTable)
                        return;
                    dtInstance = $('#datatable2').dataTable({
                        'paging': true, // Table pagination
                        'ordering': true, // Column ordering
                        'info': true, // Bottom left status text
                        'bDestroy': true,
                        // Text translation options
                        // Note the required keywords between underscores (e.g _MENU_)
                        oLanguage: {
                            sSearch: 'Search all columns:',
                            sLengthMenu: '_MENU_ records per page',
                            info: 'Showing page _PAGE_ of _PAGES_',
                            zeroRecords: 'Nothing found - sorry',
                            infoEmpty: 'No records available',
                            infoFiltered: '(filtered from _MAX_ total records)'
                        }
                    });
                    var inputSearchClass = 'datatable_input_col_search';
                    var columnInputs = $('tfoot .' + inputSearchClass);

                    // On input keyup trigger filtering
                    columnInputs
                        .keyup(function () {
                            dtInstance.fnFilter(this.value, columnInputs.index(this));
                        });
                });

                // When scope is destroyed we unload all DT instances
                // Also ColVis requires special attention since it attaches
                // elements to body and will not be removed after unload DT
                $scope.$on('$destroy', function () {
                    dtInstance.fnDestroy();
                    $('[class*=ColVis]').remove();
                });
            });

        });
    };

    getServiceList();

    // Cancel Dialog
    $scope.cancelSession = function (userid) {
        $scope.dele_val = userid;
        $scope.value = true;
        $scope.addTeam = {};
        ngDialog.open({
            template: 'app/views/cancel-dialog.html',
            className: 'ngdialog-theme-default',
            scope: $scope
        });
    };

    $scope.cancel = function () {

        $.post(MY_CONSTANT.url + '/admin_cancel_booking',
            {
                access_token: $cookieStore.get('obj').accesstoken,
                booking_id: $scope.dele_val
            },
            function (data) {
                $window.location.reload();

            });

    };

});

App.controller('UpcomingSessionController', function ($scope, $http, $location, $cookies, $cookieStore, MY_CONSTANT, $timeout, $window, ngDialog,$filter) {
    'use strict';
    $scope.loading = true;

    var getServiceList = function () {
        $.post(MY_CONSTANT.url + '/upcoming_booking', {
            access_token: $cookieStore.get('obj').accesstoken

        }, function (data) {
            var dataArray = [];
            data = JSON.parse(data);
            /*data = data.bookings;*/
            console.log(data);
            if (data.error) {
                ngDialog.open({
                    template: '<p>Something went wrong !</p>',
                    className: 'ngdialog-theme-default',
                    plain: true,
                    showClose: false,
                    closeByDocument: false,
                    closeByEscape: false
                });
                return false;
            }
            data.forEach(function (column) {

                var d = {
                    id: "",
                    technician_id: "",
                    address: "",
                    customer_name: "",
                    artist_name: "",
                    service_date: "",
                    start_time: "",
                    status: "",
                    cost: "",
                    category: "",
                    pay_to:"",
                    total_price:"",
                    usedCredits:""
                };
                var date = $filter('date')(column.service_date,'yyyy-MM-dd');
                var startTimeHours = column.start_time.split(":")[0];
                var startTimeMinutes = column.start_time.split(":")[1];
                var suffix = startTimeHours >= 12 ? "PM" : "AM",
                    hours12 = startTimeHours % 12;
                var displayTime = hours12 + ":" + startTimeMinutes + " " + suffix;
                d.id = column.cart_id;
                d.technician_id = column.technician_id;
                d.address = column.address;
                d.customer_name = column.customer_name;
                d.artist_name = column.artist_name;
                d.start_time = displayTime;
                d.cost = column.cost;
                d.category = column.category;
                d.service_date = date;
                d.status = column.status;
                d.total_price = column.total_price;
                d.usedCredits = column.credits_used;
                if(column.remaining_price==0){
                    d.pay_to = column.pay_to;
                }
                else{
                    d.pay_to = column.pay_after_promo_code;
                }
                dataArray.push(d);

            });

            $scope.$apply(function () {
                $scope.list = dataArray;


                // Define global instance we'll use to destroy later
                var dtInstance;
                $scope.loading = false;
                $timeout(function () {
                    if (!$.fn.dataTable)
                        return;
                    dtInstance = $('#datatable2').dataTable({
                        'paging': true, // Table pagination
                        'ordering': true, // Column ordering
                        'info': true, // Bottom left status text
                        'bDestroy': true,
                        // Text translation options
                        // Note the required keywords between underscores (e.g _MENU_)
                        oLanguage: {
                            sSearch: 'Search all columns:',
                            sLengthMenu: '_MENU_ records per page',
                            info: 'Showing page _PAGE_ of _PAGES_',
                            zeroRecords: 'Nothing found - sorry',
                            infoEmpty: 'No records available',
                            infoFiltered: '(filtered from _MAX_ total records)'
                        }
                    });
                    var inputSearchClass = 'datatable_input_col_search';
                    var columnInputs = $('tfoot .' + inputSearchClass);

                    // On input keyup trigger filtering
                    columnInputs
                        .keyup(function () {
                            dtInstance.fnFilter(this.value, columnInputs.index(this));
                        });
                });

                // When scope is destroyed we unload all DT instances
                // Also ColVis requires special attention since it attaches
                // elements to body and will not be removed after unload DT
                $scope.$on('$destroy', function () {
                    dtInstance.fnDestroy();
                    $('[class*=ColVis]').remove();
                });
            });

        });
    };

    getServiceList();

    // Change Status Dialog
    $scope.assignArtist = function (bookingId, artistId) {
        $scope.artistId = artistId;
        $scope.bookingId = bookingId;
        $scope.value = true;
        $scope.addTeam = {};
        ngDialog.open({
            template: 'app/views/assign-dialog.html',
            className: 'ngdialog-theme-default',
            scope: $scope
        });
    };

    $scope.assign = function () {

        $.post(MY_CONSTANT.url + '/assign_new_artist',
            {


                access_token: $cookieStore.get('obj').accesstoken,
                booking_id: $scope.bookingId,
                artist_id: $scope.artistId
            },
            function (data) {
                console.log(data);
                $window.location.reload();

            });
    };

    // Cancel Dialog
    $scope.cancelSession = function (userid) {
        console.log(userid);
        $scope.dele_val = userid;
        $scope.value = true;
        $scope.addTeam = {};
        ngDialog.open({
            template: 'app/views/cancel-dialog.html',
            className: 'ngdialog-theme-default',
            scope: $scope
        });
    };

    $scope.cancel = function () {


        $.post(MY_CONSTANT.url + '/admin_cancel_booking',
            {
                access_token: $cookieStore.get('obj').accesstoken,
                cart_id: $scope.dele_val
            },
            function (data) {
                console.log(data);
                $window.location.reload();

            });

    };

});

App.controller('PastSessionController', function ($scope, $http, $location, $cookies, $cookieStore, MY_CONSTANT, $timeout, ngDialog, $window, convertdatetime,$filter) {
    'use strict';
    $scope.loading = true;

    var getServiceList = function () {
        $.post(MY_CONSTANT.url + '/past_booking', {
                access_token: $cookieStore.get('obj').accesstoken

            }, function (data) {
                var dataArray = [];
                data = JSON.parse(data);
                console.log(data);
                if (data.error) {
                    ngDialog.open({
                        template: '<p>Something went wrong !</p>',
                        className: 'ngdialog-theme-default',
                        plain: true,
                        showClose: false,
                        closeByDocument: false,
                        closeByEscape: false
                    });
                    return false;
                }
                data.forEach(function (column) {

                    var d = {
                        id: "",
                        technician_id: "",
                        address: "",
                        customer_name: "",
                        artist_name: "",
                        service_date: "",
                        start_time: "",
                        end_time: "",
                        actual_start_time: "",
                        actual_end_time: "",
                        rating: "",
                        payable_amount: "",
                        amount_refunded: "",
                        total_price:""
                    };

                    var actualStartdisplayTime = "-";
                    var actualEnddisplayTime = "-";
                    var date =$filter('date')(column.service_date,'yyyy-MM-dd');
                    console.log(column.start_time);
                    var startTimeHours = column.start_time.split(":")[0];
                    var startTimeMinutes = column.start_time.split(":")[1];
                    var startsuffix = startTimeHours >= 12 ? "PM" : "AM",
                        starthours12 = startTimeHours % 12;
                    var startdisplayTime = starthours12 + ":" + startTimeMinutes + " " + startsuffix;
                    var endTimeHours = column.end_time.split(":")[0];
                    var endTimeMinutes = column.end_time.split(":")[1];
                    var endsuffix = endTimeHours >= 12 ? "PM" : "AM",
                        endhours12 = endTimeHours % 12;
                    var enddisplayTime = endhours12 + ":" + endTimeMinutes + " " + endsuffix;
                    if (column.actual_local_start_time != "" && column.actual_local_start_time != "0000-00-00 00:00:00") {
                        console.log(coulumn.actual_local_start_time);
                        var actualStartdisplayTime = convertdatetime.convertDateTime(column.actual_local_start_time);
                    }
                    if (column.actual_local_end_time != "" && column.actual_local_end_time != "0000-00-00 00:00:00") {
                            console.log(coulumn.actual_local_end_time);
                        var actualEnddisplayTime = convertdatetime.convertDateTime(column.actual_local_end_time);
                    }
                    var reamining_price = 0;
                    if(column.remaining_price==0){
                        reamining_price = column.pay_to;
                    }
                    else{
                        reamining_price = column.pay_after_promo_code;
                    }


                    d.id = column.cart_id;
                    d.address = column.address;
                    d.customer_name = column.customer_name;
                    d.artist_name = column.artist_name;
                    d.start_time = startdisplayTime;
                    d.end_time = enddisplayTime;
                    d.actual_start_time = actualStartdisplayTime;
                    d.actual_end_time = actualEnddisplayTime;
                    d.payable_amount = reamining_price;
                    d.amount_refunded = column.credits_used;
                    d.service_date = date;
                    d.rating = column.rating;
                    d.total_price = column.total_price;
                    dataArray.push(d);

                });

                $scope.$apply(function () {
                    $scope.list = dataArray;


                    // Define global instance we'll use to destroy later
                    var dtInstance;
                    $scope.loading = false;
                    $timeout(function () {
                        if (!$.fn.dataTable)
                            return;
                        dtInstance = $('#datatable2').dataTable({
                            'paging': true, // Table pagination
                            'ordering': true, // Column ordering
                            'info': true, // Bottom left status text
                            'bDestroy': true,
                            // Text translation options
                            // Note the required keywords between underscores (e.g _MENU_)
                            oLanguage: {
                                sSearch: 'Search all columns:',
                                sLengthMenu: '_MENU_ records per page',
                                info: 'Showing page _PAGE_ of _PAGES_',
                                zeroRecords: 'Nothing found - sorry',
                                infoEmpty: 'No records available',
                                infoFiltered: '(filtered from _MAX_ total records)'
                            }
                        });
                        var inputSearchClass = 'datatable_input_col_search';
                        var columnInputs = $('tfoot .' + inputSearchClass);

                        // On input keyup trigger filtering
                        columnInputs
                            .keyup(function () {
                                dtInstance.fnFilter(this.value, columnInputs.index(this));
                            });
                    });

                    // When scope is destroyed we unload all DT instances
                    // Also ColVis requires special attention since it attaches
                    // elements to body and will not be removed after unload DT
                    $scope.$on('$destroy', function () {
                        dtInstance.fnDestroy();
                        $('[class*=ColVis]').remove();
                    });
                });

            }
        )
        ;
    };

    getServiceList();
})
;

App.controller('RejectedSessionController', function ($scope, $http, $location, $cookies, $cookieStore, MY_CONSTANT, $timeout, $window, ngDialog) {
    'use strict';
    $scope.loading = true;

    var getServiceList = function () {
        $.post(MY_CONSTANT.url + '/upcoming_rejected_booking', {
            access_token: $cookieStore.get('obj').accesstoken

        }, function (data) {
            var dataArray = [];
            data = JSON.parse(data);
            console.log(data);
            if (data.error) {
                ngDialog.open({
                    template: '<p>Something went wrong !</p>',
                    className: 'ngdialog-theme-default',
                    plain: true,
                    showClose: false,
                    closeByDocument: false,
                    closeByEscape: false
                });
                return false;
            }
            data.forEach(function (column) {

                var d = {
                    id: "",
                    booking_id: "",
                    service_id: "",
                    technician_id: "",
                    address: "",
                    customer_name: "",
                    artist_name: "",
                    service_date: "",
                    start_time: "",
                    status: "",
                    cost: "",
                    payable_amount: "",
                    category: "",
                    service_name: "",
                    reason: ""
                };

                var date = column.service_date.toString().split("T")[0];
                var startTimeHours = column.start_time.split(":")[0];
                var startTimeMinutes = column.start_time.split(":")[1];
                var suffix = startTimeHours >= 12 ? "PM" : "AM",
                    hours12 = startTimeHours % 12;
                var displayTime = hours12 + ":" + startTimeMinutes + " " + suffix;
                d.id = column.id;
                d.booking_id = column.booking_id;
                d.service_id = column.service_id;
                d.technician_id = column.technician_id;
                d.address = column.address;
                d.customer_name = column.customer_name;
                d.artist_name = column.artist_name;
                d.start_time = displayTime;
                d.cost = column.cost;
                d.payable_amount = column.payable_amount;
                d.category = column.category;
                d.service_date = date;
                d.status = column.status;
                d.service_name = column.service_name;
                d.reason = column.reason;
                dataArray.push(d);

            });

            $scope.$apply(function () {
                $scope.list = dataArray;


                // Define global instance we'll use to destroy later
                var dtInstance;
                $scope.loading = false;
                $timeout(function () {
                    if (!$.fn.dataTable)
                        return;
                    dtInstance = $('#datatable2').dataTable({
                        'paging': true, // Table pagination
                        'ordering': true, // Column ordering
                        'info': true, // Bottom left status text
                        'bDestroy': true,
                        // Text translation options
                        // Note the required keywords between underscores (e.g _MENU_)
                        oLanguage: {
                            sSearch: 'Search all columns:',
                            sLengthMenu: '_MENU_ records per page',
                            info: 'Showing page _PAGE_ of _PAGES_',
                            zeroRecords: 'Nothing found - sorry',
                            infoEmpty: 'No records available',
                            infoFiltered: '(filtered from _MAX_ total records)'
                        }
                    });
                    var inputSearchClass = 'datatable_input_col_search';
                    var columnInputs = $('tfoot .' + inputSearchClass);

                    // On input keyup trigger filtering
                    columnInputs
                        .keyup(function () {
                            dtInstance.fnFilter(this.value, columnInputs.index(this));
                        });
                });

                // When scope is destroyed we unload all DT instances
                // Also ColVis requires special attention since it attaches
                // elements to body and will not be removed after unload DT
                $scope.$on('$destroy', function () {
                    dtInstance.fnDestroy();
                    $('[class*=ColVis]').remove();
                });
            });

        });
    };

    getServiceList();

    $scope.assignArtist = function (bookingId, artistId) {
        var retVal = confirm("Do you want to assign this artist to this session ?");
        if (retVal == true) {
            $.post(MY_CONSTANT.url + '/assign_new_artist',
                {
                    access_token: $cookieStore.get('obj').accesstoken,
                    booking_id: bookingId,
                    artist_id: artistId
                },
                function (data) {

                    data = JSON.parse(data);
                    $window.location.reload();

                });
            $scope.reloadRoute();
            return true;

        } else {
            $scope.selectedArtistId = 'default';
            return false;
        }
    };

    $scope.cancelSession = function (bookingId) {
        var retVal = confirm("Do you want to cancel this booking?");
        if (retVal == true) {
            $.post(MY_CONSTANT.url + '/admin_cancel_booking',
                {
                    access_token: $cookieStore.get('obj').accesstoken,
                    booking_id: bookingId
                },
                function (data) {

                    data = JSON.parse(data);
                    $window.location.reload();

                });
            $scope.reloadRoute();
            return true;

        } else {
            $scope.selectedArtistId = 'default';
            return false;
        }
    };

});

App.controller('TodayBookingController', function ($scope,$route, $http, $location, $cookies, $cookieStore, MY_CONSTANT, $timeout, $window, ngDialog,$filter) {
    'use strict';
    $scope.loading = true;

    $scope.booking_date = $filter('date')(new Date(),'yyyy-MM-dd');

    $scope.minDate = $filter('date')(new Date(),'yyyy-MM-dd');


    getPageData($scope.booking_date);


    //according to date get the booking data
    $scope.changeDate = function(date){
        var changeDate1=$filter('date')(date,'yyyy-MM-dd');
        console.log(changeDate1);
        getPageData(changeDate1);
    }

    $scope.changeCategoryStatus=function(category,id){
        if(category==0){
            alert("plz select the category status");
            return false;
        }
        changeBookingStatus(category,id);
    }

    function changeBookingStatus(category,id){

        console.log("change booking"+category,id);

        $.post(MY_CONSTANT.url + '/change_booking_status', {

            access_token: $cookieStore.get('obj').accesstoken,
            change_status:category,
            id:id

        }, function (data) {

            data=JSON.parse(data);
            console.log(data);
            if(data.log){
                ngDialog.open({
                    template: '<p class="del-dialog">'+data.log+'</p>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                });
                $timeout(function() {
                    location.reload(true);
                }, 1000);
            }
            else{
                alert("something wrong ");
            }
        });
    }

    function getPageData(date){
        $.post(MY_CONSTANT.url + '/get_cart_booking', {

            access_token: $cookieStore.get('obj').accesstoken,
            date:date

        }, function (data) {
            console.log(data);
            var dataArray = [];
            var i=1;
            $scope.selectedArtistId = 'default';
            data = JSON.parse(data);
            console.log(data);
            var data_bookings=data;
            console.log(data_bookings)
            data_bookings.forEach(function (column) {
                var d = {
                    id: "",
                    technician_id: "",
                    address: "",
                    customer_name: "",
                    artist_name: "",
                    service_date: "",
                    start_time: "",
                    status: "",
                    category: "",
                    disabled:"",
                    acceptBooking:"",
                    endBooking:"",
                    declineBooking:"",
                    pay_to:"",
                    total_price:"",
                    usedCredits:"",
                    promoCodeDiscountPrice:"",
                    numberOfArtist:""
                };
                /* var date = column.service_date.toString().split("T")[0];*/
                var startTimeHours = column.start_time.split(":")[0];
                var startTimeMinutes = column.start_time.split(":")[1];
                var suffix = startTimeHours >= 12 ? "PM" : "AM",
                    hours12 = "";
                if(startTimeHours==12){
                    hours12=startTimeHours;
                }else{
                    hours12 = startTimeHours % 12;
                }
                var displayTime = hours12+":"+startTimeMinutes+" "+suffix;
                d.id = column.cart_id;
                d.cart_id = column.cart_id;
                d.technician_id = column.technician_id;
                d.address = column.address;
                d.customer_name = column.customer_name;
                d.artist_name = column.artist_name;

                d.start_time = displayTime;
                d.category = column.category;
                d.service_date =$filter('date')(column.service_date,'yyyy-MM-dd');
                d.status = column.status;
                d.total_price = column.total_price;
                d.usedCredits = column.credits_used;
                d.promoCodeDiscountPrice = column.promo_code_discount;
                d.numberOfArtist = column.number_of_persons;
                if(column.remaining_price==0){
                    d.pay_to = column.pay_to;
                }
                else{
                    d.pay_to = column.pay_after_promo_code;
                }

                if(column.status==8||column.status==7||column.status==3||column.status==4||column.status==5||column.status==6){
                    d.disabled=true;
                }
                else{
                    d.disabled=false;
                }
                if(column.status==1){
                    d.acceptBooking=false;
                    d.declineBooking=false;
                    d.endBooking=true;
                }
                if(column.status==2){
                    d.acceptBooking=true;
                    d.endBooking=false;
                    d.declineBooking=false;
                }
                dataArray.push(d);

            });

            $scope.$apply(function () {

                $scope.list = dataArray;
                // Define global instance we'll use to destroy later
                if ($.fn.DataTable.isDataTable("#datatable2")) {
                    console.log("hello");
                    $('#datatable2').DataTable().clear().destroy();
                }
                var dtInstance;
                console.log(dtInstance);
                $scope.loading = false;
                $timeout(function () {
                    if (!$.fn.dataTable)
                        return;
                    dtInstance = $('#datatable2').dataTable({
                        'paging': true, // Table pagination
                        'ordering': true, // Column ordering
                        'info': true, // Bottom left status text
                        'bDestroy': true,
                        // Text translation options
                        // Note the required keywords between underscores (e.g _MENU_)
                        oLanguage: {
                            sSearch: 'Search all columns:',
                            sLengthMenu: '_MENU_ records per page',
                            info: 'Showing page _PAGE_ of _PAGES_',
                            zeroRecords: 'Nothing found - sorry',
                            infoEmpty: 'No records available',
                            infoFiltered: '(filtered from _MAX_ total records)'
                        }
                    });
                    var inputSearchClass = 'datatable_input_col_search';
                    var columnInputs = $('tfoot .' + inputSearchClass);

                    // On input keyup trigger filtering
                    columnInputs
                        .keyup(function () {
                            dtInstance.fnFilter(this.value, columnInputs.index(this));
                        });
                });

                // When scope is destroyed we unload all DT instances
                // Also ColVis requires special attention since it attaches
                // elements to body and will not be removed after unload DT
                $scope.$on('$destroy', function () {
                    dtInstance.fnDestroy();
                    $('[class*=ColVis]').remove();
                });
            });

        });

    }
});

App.controller('CartDetailsController', function ($scope, $http, $location, $cookies, $cookieStore, MY_CONSTANT, ngDialog, $timeout, $stateParams) {
    'use strict';
    $scope.loading = true;

    console.log($stateParams.id);


    $.post(MY_CONSTANT.url + '/cart_details', {
        access_token: $cookieStore.get('obj').accesstoken,
        cart_id: $stateParams.id

    }, function (data) {
        var dataArray = [];
        data = JSON.parse(data);
       // console.log(data);
        if (data.error) {
            ngDialog.open({
                template: '<p>Something went wrong !</p>',
                className: 'ngdialog-theme-default',
                plain: true,
                showClose: true,
                closeByDocument: true,
                closeByEscape: false
            });
            return false;
        }
        data.forEach(function (column) {

            var d = {
                status: "",
                cost: "",
                category: "",
                service_name: "",
                quantity: "",
                service_time:""
            };
            d.cost = column.cost;
            d.category = column.category;
            d.status = column.status;
            d.service_name = column.service_name;
            d.quantity = column.quantity;
            d.service_time = column.service_time;
            dataArray.push(d);

        });

        console.log("print the data array"+dataArray);

        $scope.$apply(function () {

            $scope.list = dataArray;


            // Define global instance we'll use to destroy later
            var dtInstance;
            $scope.loading = false;
            $timeout(function () {
                if (!$.fn.dataTable)
                    return;
                dtInstance = $('#dataTable2').dataTable({
                    'paging': true, // Table pagination
                    'ordering': true, // Column ordering
                    'info': true, // Bottom left status text
                    'bDestroy': true,
                    // Text translation options
                    // Note the required keywords between underscores (e.g _MENU_)
                    oLanguage: {
                        sSearch: 'Search all columns:',
                        sLengthMenu: '_MENU_ records per page',
                        info: 'Showing page _PAGE_ of _PAGES_',
                        zeroRecords: 'Nothing found - sorry',
                        infoEmpty: 'No records available',
                        infoFiltered: '(filtered from _MAX_ total records)'
                    }
                });
                var inputSearchClass = 'datatable_input_col_search';
                var columnInputs = $('tfoot .' + inputSearchClass);

                // On input keyup trigger filtering
                columnInputs
                    .keyup(function () {
                        dtInstance.fnFilter(this.value, columnInputs.index(this));
                    });
            });

            // When scope is destroyed we unload all DT instances
            // Also ColVis requires special attention since it attaches
            // elements to body and will not be removed after unload DT
            $scope.$on('$destroy', function () {
                dtInstance.fnDestroy();
                $('[class*=ColVis]').remove();
            });
        });

    });
});

